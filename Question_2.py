"""
s1 = "Hello"
s2 = "vufigsaHudesdflelio"


# l1 = list(s1)
# l2 = list(s2)

s3 = ""

for letter in s2:
    if letter in s1:
        s3 += letter
    else:
        s2.replace(letter," ")

print(s3)


# --------- correction

counter = 0
for ch in s1.lower():
    if ch in s2.lower():
        counter += 1 #counter = counter + 1

if counter == len(s1):
    print(True)
else:
    print(False)
    
# OR you can use class called Counter

print(Counter(s2))
"""