"""
class Calculator:

    def __init__(self, num1, num2):
        self.num1 = num1
        self.num2 = num2

    def add(self):
        return self.num1 + self.num2

    def sub(self):
        return self.num1 - self.num2

    def multi(self):
        return self.num1 * self.num2

    def divi(self):
        return self.num1 / self.num2

    def root(self):
        return self.num1 ** self.num2
"""
"""reset memory not done"""

# ----------- correction

from math import sqrt

class Calculator:

    def __init__(self):
        self.memory = 0

    def add(self, number):
        self.memory += number
        return self.memory

    def substraction(self, number):
        self.memory -= number
        return self.memory

    def multiply(self,number):
        self.memory *= number
        return self.memory

    def division(self, number):
        self.memory /= number
        return self.memory

    def square_root(self):
        self.memory = sqrt(self.memory)
        return self.memory

    def reset(self):
        self.memory = 0

calc =  Calculator()
print(calc.add(10))
print(calc.add(5.30))
print(calc.square_root())
calc.reset()
print(calc.add(2))