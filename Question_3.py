nums = [1,2,3,4,5,6,7,8,-9999,8,7,6,5,4,3,2,1]

def sum_num_list(nums:list):
    sum = 0
    for x in nums:
        if x != -9999:
            sum += x # sum = sum + x
        else:
            break
    return sum

print(sum_num_list(nums))